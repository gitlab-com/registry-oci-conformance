![pipeline](https://gitlab.com/gitlab-com/registry-oci-conformance/badges/main/pipeline.svg?key_text=OCI%20Conformance%20&key_width=110)


This repository runs the [OCI Distribution Conformance](https://github.com/opencontainers/distribution-spec/tree/main/conformance) tooling in GitLab CI/CD to test the GitLab.com Container Registry.

The `OCI Conformance` badge at the top of this README indicates whether conformance tests have passed or not based on the latest CI/CD pipeline, which is scheduled to run daily.

To see the latest test report:

1. Go to to the [project pipelines list](https://gitlab.com/gitlab-com/registry-oci-conformance/-/pipelines/latest), and select the latest pipeline.
1. From the latest pipeline's details page, select the **Tests** tab.
1. Select `oci-conformance`. You should see the full list of executed tests, including any details for any failures.

Related to the [Container Registry OCI Conformance epic (&10345)](https://gitlab.com/groups/gitlab-org/-/epics/10345).
